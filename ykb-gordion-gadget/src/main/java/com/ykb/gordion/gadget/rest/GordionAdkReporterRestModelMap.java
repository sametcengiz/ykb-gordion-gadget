/**@author Samet Cengiz
 * */
package com.ykb.gordion.gadget.rest;

import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "gordionDataMap")
@XmlAccessorType(XmlAccessType.FIELD)
public class GordionAdkReporterRestModelMap {
	
@XmlElement(name = "gordionDataMap")	
 private List<GordionAdkReporterRestModel>gordionDataMap;

public List<GordionAdkReporterRestModel> getGordionDataMap() {
	return gordionDataMap;
}

public void setGordionDataMap(List<GordionAdkReporterRestModel> gordionDataMap) {
	this.gordionDataMap = gordionDataMap;
}
}
