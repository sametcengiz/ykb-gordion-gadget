/**
 * @author Samet Cengiz
 * */
package com.ykb.gordion.gadget.utils;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.atlassian.jira.bc.issue.search.SearchService;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.search.SearchException;
import com.atlassian.jira.issue.search.SearchResults;
import com.atlassian.jira.jql.builder.JqlClauseBuilder;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.bean.PagerFilter;
import com.atlassian.plugin.util.ClassLoaderUtils;
import com.atlassian.query.Query;
import com.ykb.gordion.gadget.rest.GordionAdkReporterRestModel;

public class GordionUtils {

	private static final Logger log = LoggerFactory.getLogger(GordionUtils.class);

	public GordionUtils() {

	}

	public static void printError(Exception e, Logger log) {
		StringWriter stack = new StringWriter();
		e.printStackTrace(new PrintWriter(stack));
		log.error(stack.toString());
	}

	public static void printDebug(Exception e, Logger log) {
		StringWriter stack = new StringWriter();
		e.printStackTrace(new PrintWriter(stack));
		log.debug(stack.toString());
	}

	public static String getErrorStack(Exception e) {
		StringWriter stack = new StringWriter();
		e.printStackTrace(new PrintWriter(stack));
		return stack.toString();
	}

	public static List<Issue> getIssuesFromJql(String project, String issueType, ApplicationUser user) {
		List<Issue> issueList = null;

		try {
			JqlClauseBuilder jqlClauseBuilder = JqlQueryBuilder.newClauseBuilder();
			SearchService searchService = ComponentAccessor.getComponent(SearchService.class);
			jqlClauseBuilder.project(project).and().issueType(issueType);
			Query query = jqlClauseBuilder.buildQuery();
			log.info("Your jql query is: " + query);
			SearchResults searchResults = null;

			try {

				searchResults = searchService.search(user.getDirectoryUser(), query, PagerFilter.getUnlimitedFilter());

			} catch (SearchException e) {
				printError(e, log);
			}

			if (searchResults != null && searchResults.getIssues() != null && searchResults.getIssues().size() > 0) {
				issueList = searchResults.getIssues();
			}
		} catch (Exception e) {
			printError(e, log);
		}
		return issueList;
	}

	public static Map<String, List<Issue>> decomposeIssuesByResponsibleDevelopmentDepartment(
			CustomFieldManager customFieldManager, List<Issue> issueList, String customFiledId) {
		Map<String, List<Issue>> recordDepartmentMap = new LinkedHashMap<String, List<Issue>>();

		if (issueList != null && issueList.size() > 0) {
			for (Issue issue : issueList) {
				String departmentName = readCustomField(customFieldManager, customFiledId, issue, true);
				if (recordDepartmentMap.containsKey(departmentName)) {
					recordDepartmentMap.get(departmentName).add(issue);
				} else {
					List<Issue> subList = new ArrayList<Issue>();
					subList.add(issue);
					recordDepartmentMap.put(departmentName, subList);
				}
			}
		}
		return recordDepartmentMap;
	}

	public static String readCustomField(CustomFieldManager customFieldManager, String customfieldID, Issue issue,
			Boolean isOption) {
		String customFieldValue = "";

		try {
			CustomField customField = customFieldManager.getCustomFieldObject(customfieldID);
			if (isOption) {
				Option option = (Option) issue.getCustomFieldValue(customField);

				if (option != null) {
					customFieldValue = option.getValue();
				} else {
					customFieldValue = (String) issue.getCustomFieldValue(customField);
				}
			}

		} catch (Exception e) {
			printError(e, log);
		}

		return customFieldValue;
	}

	public static Map<String, Object> initializeNewRow() {
		Map<String, Object> rowMap = new LinkedHashMap<String, Object>();
		rowMap.put("responsibleDepartment", "");
		rowMap.put("openStatus", 0);
		rowMap.put("inDevelopmentStatus", 0);
		rowMap.put("unitTestStatus", 0);
		rowMap.put("functionalTestStatus", 0);
		rowMap.put("inReviewStatus", 0);
		rowMap.put("integrationStatus", 0);
		rowMap.put("testStatus", 0);
		rowMap.put("uatStatus", 0);
		rowMap.put("liveStatus", 0);
		return rowMap;
	}

	public static void incrementDetectedStatus(Issue issue, Map<String, Object> rowMap) {
		Map<String, String> fieldMap = GordionUtils.loadProperties();

		if (issue.getStatusObject().getId().equals(fieldMap.get("openStatusID"))) {
			rowMap.put("openStatus", incrementIssueNumber(rowMap.get("openStatus")));
		}
		if (issue.getStatusObject().getId().equals(fieldMap.get("inProgressStatusID"))) {
			rowMap.put("inDevelopmentStatus", incrementIssueNumber(rowMap.get("inDevelopmentStatus")));
		}
		if (issue.getStatusObject().getId().equals(fieldMap.get("unitTestStatusID"))) {
			rowMap.put("unitTestStatus", incrementIssueNumber(rowMap.get("unitTestStatus")));
		}

		if (issue.getStatusObject().getId().equals(fieldMap.get("functionalTestStatusID"))) {
			rowMap.put("functionalTestStatus", incrementIssueNumber(rowMap.get("functionalTestStatus")));
		}

		if (issue.getStatusObject().getId().equals(fieldMap.get("inReviewStatusID"))) {
			rowMap.put("inReviewStatus", incrementIssueNumber(rowMap.get("inReviewStatus")));
		}

		if (issue.getStatusObject().getId().equals(fieldMap.get("integrationStatusID"))) {
			rowMap.put("integrationStatus", incrementIssueNumber(rowMap.get("integrationStatus")));
		}

		if (issue.getStatusObject().getId().equals(fieldMap.get("testStatusID"))) {
			rowMap.put("testStatus", incrementIssueNumber(rowMap.get("testStatus")));
		}

		if (issue.getStatusObject().getId().equals(fieldMap.get("uatStatusID"))) {
			rowMap.put("uatStatus", incrementIssueNumber(rowMap.get("uatStatus")));
		}

		if (issue.getStatusObject().getId().equals(fieldMap.get("liveStatusID"))) {
			rowMap.put("liveStatus", incrementIssueNumber(rowMap.get("liveStatus")));
		}

	}

	public static int incrementIssueNumber(Object currentNo) {
		Integer currentNumber = (Integer) currentNo;
		return currentNumber + 1;
	}

	public static List<Map<String, Object>> createRowMapOfGadgetTable(Map<String, List<Issue>> departmentRelatedMap) {
		List<Map<String, Object>> mapList = new ArrayList<Map<String, Object>>();

		for (String relatedDepartment : departmentRelatedMap.keySet()) {
			Map<String, Object> row = initializeNewRow();
			row.put("responsibleDepartment", relatedDepartment);
			for (Issue issue : departmentRelatedMap.get(relatedDepartment)) {
				incrementDetectedStatus(issue, row);

			}
			mapList.add(row);
		}
		return mapList;
	}

	public static List<GordionAdkReporterRestModel> createRestServiceModelList(List<Map<String, Object>> mapList) {
		List<GordionAdkReporterRestModel> modelList = new ArrayList<GordionAdkReporterRestModel>();

		for (Map<String, Object> map : mapList) {
			GordionAdkReporterRestModel model = new GordionAdkReporterRestModel();
			model.setResponsibleDepartmentName((String) map.get("responsibleDepartment"));
			model.setOpenRecordCount((Integer) map.get("openStatus"));
			model.setInDevelopmentStatusRecordCount((Integer) map.get("inDevelopmentStatus"));
			model.setUnitTestStatusRecordCount((Integer) map.get("unitTestStatus"));
			model.setFunctionalTestRecordCount((Integer) map.get("functionalTestStatus"));
			model.setInReviewStatusRecordCount((Integer) map.get("inReviewStatus"));
			model.setIntegrationStatusRecordCount((Integer) map.get("integrationStatus"));
			model.setTestStatusRecordCount((Integer) map.get("testStatus"));
			model.setUatStatusRecordCount((Integer) map.get("uatStatus"));
			model.setLiveStatusRecordCount((Integer) map.get("liveStatus"));
			calculateTotalRecordCount(model);
			formatPercentageOfModel(model);
			modelList.add(model);
		}
		return modelList;
	}

	
	public static void formatPercentageOfModel(GordionAdkReporterRestModel gordionModel)
	{
		if(gordionModel!=null)
		{
			
			float openRecordPercent=((float)gordionModel.getOpenRecordCount()/gordionModel.getTotalRecordCountOfDepartment())*100;
			gordionModel.setOpentStatusRecordPercent(formatFloatingPoint(openRecordPercent));
			float inDevelopmentPercent=((float)gordionModel.getInDevelopmentStatusRecordCount()/gordionModel.getTotalRecordCountOfDepartment())*100;
			gordionModel.setInDevelopmentStatusPercent(formatFloatingPoint(inDevelopmentPercent));
			float unitTestPercent=((float)gordionModel.getUnitTestStatusRecordCount()/gordionModel.getTotalRecordCountOfDepartment())*100;
			gordionModel.setUnitTestStatusPercent(formatFloatingPoint(unitTestPercent));	
			float functionalTestPercent=((float)gordionModel.getFunctionalTestRecordCount()/gordionModel.getTotalRecordCountOfDepartment())*100;
			gordionModel.setFunctionalTestPercent(formatFloatingPoint(functionalTestPercent));
			float inReviewPercent=((float)gordionModel.getInReviewStatusRecordCount()/gordionModel.getTotalRecordCountOfDepartment())*100;
			gordionModel.setInReviewPercent(formatFloatingPoint(inReviewPercent));
			float integrationPercent=((float)gordionModel.getIntegrationStatusRecordCount()/gordionModel.getTotalRecordCountOfDepartment())*100;
			gordionModel.setIntegrationPercent(formatFloatingPoint(integrationPercent));
			float testPercent=((float)gordionModel.getTestStatusRecordCount()/gordionModel.getTotalRecordCountOfDepartment())*100;
			gordionModel.setTestPercent(formatFloatingPoint(testPercent));
			float uatPercent=((float)gordionModel.getUatStatusRecordCount()/gordionModel.getTotalRecordCountOfDepartment())*100;
			gordionModel.setUatPercent(formatFloatingPoint(uatPercent));
			float livePercent=((float)gordionModel.getLiveStatusRecordCount()/gordionModel.getTotalRecordCountOfDepartment())*100;
			gordionModel.setLivePercent(formatFloatingPoint(livePercent));
			
				
		}
	}
	
	public static String formatFloatingPoint(float value)
	{
		DecimalFormat formatter=new DecimalFormat("#.#"); 
		return formatter.format(value);
	}
	
	
	public static int calculateTotalRecordCount(GordionAdkReporterRestModel model) {
		int total = 0;

		if (model != null) {
			total = model.getOpenRecordCount() + model.getInDevelopmentStatusRecordCount()
					+ model.getUnitTestStatusRecordCount() + model.getFunctionalTestRecordCount()
					+ model.getInReviewStatusRecordCount() + model.getIntegrationStatusRecordCount()
					+ model.getTestStatusRecordCount() + model.getUatStatusRecordCount()
					+ model.getLiveStatusRecordCount();
			model.setTotalRecordCountOfDepartment(total);
		}

		return total;
	}
	
	
	public static Map<String, String> loadProperties() {
		Map<String, String> fieldMap = new HashMap<String, String>();
		Properties property = new Properties();
		InputStream input = null;
		try {

			input = ClassLoaderUtils.getResourceAsStream("ykb-gordion-gadget.properties",
					new GordionUtils().getClass());
			property.load(input);
			fieldMap.put("issueTypeID", property.getProperty("gordion.adk.issueTypeID"));
			fieldMap.put("openStatusID", property.getProperty("gordion.adk.openStatusID"));
			fieldMap.put("relatedDepartmentCustomFieldID",
					property.getProperty("gordion.adk.relatedDepartmentCustomFieldID"));
			fieldMap.put("inProgressStatusID", property.getProperty("gordion.adk.inProgressStatusID"));
			fieldMap.put("unitTestStatusID", property.getProperty("gordion.adk.unitTestStatusID"));
			fieldMap.put("functionalTestStatusID", property.getProperty("gordion.adk.functionalTestStatusID"));
			fieldMap.put("inReviewStatusID", property.getProperty("gordion.adk.inReviewStatusID"));
			fieldMap.put("integrationStatusID", property.getProperty("gordion.adk.integrationStatusID"));
			fieldMap.put("testStatusID", property.getProperty("gordion.adk.testStatusID"));
			fieldMap.put("uatStatusID", property.getProperty("gordion.adk.uatStatusID"));
			fieldMap.put("liveStatusID", property.getProperty("gordion.adk.liveStatusID"));
			fieldMap.put("projectKey", property.getProperty("gordion.adk.projectKey"));
		} catch (Exception e) {
			printError(e, log);
		}
		return fieldMap;
	}

}
