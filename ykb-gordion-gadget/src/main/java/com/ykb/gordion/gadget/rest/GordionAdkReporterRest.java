/**@author Samet Cengiz
 * 
 * */
package com.ykb.gordion.gadget.rest;
import java.util.List;
import java.util.Map;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.ykb.gordion.gadget.utils.GordionUtils;


@Path("/message")
public class GordionAdkReporterRest {

	private final CustomFieldManager customFieldManager;
	private ApplicationUser user;
	
	public GordionAdkReporterRest(CustomFieldManager customFieldManager)
	{
		this.customFieldManager=customFieldManager;	
		this.user=ComponentAccessor.getJiraAuthenticationContext().getUser();
	}
	
	
    @GET
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response findGordionTasks()
    {
    
    	Map<String,String>fieldMap=GordionUtils.loadProperties();
    	List<Issue>issueList=GordionUtils.getIssuesFromJql(fieldMap.get("projectKey"), fieldMap.get("issueTypeID"), user);
    	Map<String, List<Issue>>departmentRecordMap=GordionUtils.decomposeIssuesByResponsibleDevelopmentDepartment(customFieldManager, issueList, fieldMap.get("relatedDepartmentCustomFieldID"));
    	List<Map<String,Object>>mapList=GordionUtils.createRowMapOfGadgetTable(departmentRecordMap);
    	GordionAdkReporterRestModelMap model=new GordionAdkReporterRestModelMap();
    	model.setGordionDataMap(GordionUtils.createRestServiceModelList(mapList));
        return Response.ok(model).build();
    }    
}