/**@author Samet Cengiz
 * 
 * */
package com.ykb.gordion.gadget.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "message")
@XmlAccessorType(XmlAccessType.FIELD)
public class GordionAdkReporterRestModel {

	@XmlElement(name = "responsibleDepartmentName")
	private String responsibleDepartmentName;

	@XmlElement(name = "openRecordCount")
	private int openRecordCount;

	@XmlElement(name = "inDevelopmentStatusRecordCount")
	private int inDevelopmentStatusRecordCount;

	@XmlElement(name = "unitTestStatusRecordCount")
	private int unitTestStatusRecordCount;

	@XmlElement(name = "functionalTestRecordCount")
	private int functionalTestRecordCount;

	@XmlElement(name = "inReviewStatusRecordCount")
	private int inReviewStatusRecordCount;

	@XmlElement(name = "integrationStatusRecordCount")
	private int integrationStatusRecordCount;

	@XmlElement(name = "testStatusRecordCount")
	private int testStatusRecordCount;

	@XmlElement(name = "uatStatusRecordCount")
	private int uatStatusRecordCount;

	@XmlElement(name = "liveStatusRecordCount")
	private int liveStatusRecordCount;

	@XmlElement(name = "totalRecordCountOfDepartment")
	private int totalRecordCountOfDepartment;

	@XmlElement(name = "opentStatusRecordPercent")
	private String opentStatusRecordPercent;

	@XmlElement(name = "inDevelopmentStatusPercent")
	private String inDevelopmentStatusPercent;

	@XmlElement(name = "unitTestStatusPercent")
	private String unitTestStatusPercent;

	@XmlElement(name = "functionalTestPercent")
	private String functionalTestPercent;

	@XmlElement(name = "inReviewPercent")
	private String inReviewPercent;

	@XmlElement(name = "integrationPercent")
	private String integrationPercent;

	@XmlElement(name = "testPercent")
	private String testPercent;

	@XmlElement(name = "uatPercent")
	private String uatPercent;

	@XmlElement(name = "livePercent")
	private String livePercent;

	public GordionAdkReporterRestModel() {
	}

	public GordionAdkReporterRestModel(String responsibleDepartmentName, Integer openRecordCount,
			Integer inDevelopmentStatusRecordCount, Integer unitTestStatusRecordCount,
			Integer functionalTestRecordCount, Integer inReviewStatusRecordCount, Integer integrationStatusRecordCount,
			Integer testStatusRecordCount, Integer uatStatusRecordCount, Integer liveStatusRecordCount) {
		super();
		this.responsibleDepartmentName = responsibleDepartmentName;
		this.openRecordCount = openRecordCount;
		this.inDevelopmentStatusRecordCount = inDevelopmentStatusRecordCount;
		this.unitTestStatusRecordCount = unitTestStatusRecordCount;
		this.functionalTestRecordCount = functionalTestRecordCount;
		this.inReviewStatusRecordCount = inReviewStatusRecordCount;
		this.integrationStatusRecordCount = integrationStatusRecordCount;
		this.testStatusRecordCount = testStatusRecordCount;
		this.uatStatusRecordCount = uatStatusRecordCount;
		this.liveStatusRecordCount = liveStatusRecordCount;
	}

	public Integer getInDevelopmentStatusRecordCount() {
		return inDevelopmentStatusRecordCount;
	}

	public void setInDevelopmentStatusRecordCount(Integer inDevelopmentStatusRecordCount) {
		this.inDevelopmentStatusRecordCount = inDevelopmentStatusRecordCount;
	}

	public Integer getUnitTestStatusRecordCount() {
		return unitTestStatusRecordCount;
	}

	public void setUnitTestStatusRecordCount(Integer unitTestStatusRecordCount) {
		this.unitTestStatusRecordCount = unitTestStatusRecordCount;
	}

	public Integer getFunctionalTestRecordCount() {
		return functionalTestRecordCount;
	}

	public void setFunctionalTestRecordCount(Integer functionalTestRecordCount) {
		this.functionalTestRecordCount = functionalTestRecordCount;
	}

	public Integer getInReviewStatusRecordCount() {
		return inReviewStatusRecordCount;
	}

	public void setInReviewStatusRecordCount(Integer inReviewStatusRecordCount) {
		this.inReviewStatusRecordCount = inReviewStatusRecordCount;
	}

	public Integer getIntegrationStatusRecordCount() {
		return integrationStatusRecordCount;
	}

	public void setIntegrationStatusRecordCount(Integer integrationStatusRecordCount) {
		this.integrationStatusRecordCount = integrationStatusRecordCount;
	}

	public Integer getTestStatusRecordCount() {
		return testStatusRecordCount;
	}

	public void setTestStatusRecordCount(Integer testStatusRecordCount) {
		this.testStatusRecordCount = testStatusRecordCount;
	}

	public Integer getUatStatusRecordCount() {
		return uatStatusRecordCount;
	}

	public void setUatStatusRecordCount(Integer uatStatusRecordCount) {
		this.uatStatusRecordCount = uatStatusRecordCount;
	}

	public Integer getLiveStatusRecordCount() {
		return liveStatusRecordCount;
	}

	public void setLiveStatusRecordCount(Integer liveStatusRecordCount) {
		this.liveStatusRecordCount = liveStatusRecordCount;
	}

	public String getResponsibleDepartmentName() {
		return responsibleDepartmentName;
	}

	public void setResponsibleDepartmentName(String responsibleDepartmentName) {
		this.responsibleDepartmentName = responsibleDepartmentName;
	}

	public Integer getOpenRecordCount() {
		return openRecordCount;
	}

	public void setOpenRecordCount(Integer openRecordCount) {
		this.openRecordCount = openRecordCount;
	}

	public Integer getTotalRecordCountOfDepartment() {
		return totalRecordCountOfDepartment;
	}

	public void setTotalRecordCountOfDepartment(Integer totalRecordCountOfDepartment) {
		this.totalRecordCountOfDepartment = totalRecordCountOfDepartment;
	}

	public void setOpenRecordCount(int openRecordCount) {
		this.openRecordCount = openRecordCount;
	}

	public void setInDevelopmentStatusRecordCount(int inDevelopmentStatusRecordCount) {
		this.inDevelopmentStatusRecordCount = inDevelopmentStatusRecordCount;
	}

	public void setUnitTestStatusRecordCount(int unitTestStatusRecordCount) {
		this.unitTestStatusRecordCount = unitTestStatusRecordCount;
	}

	public void setFunctionalTestRecordCount(int functionalTestRecordCount) {
		this.functionalTestRecordCount = functionalTestRecordCount;
	}

	public void setInReviewStatusRecordCount(int inReviewStatusRecordCount) {
		this.inReviewStatusRecordCount = inReviewStatusRecordCount;
	}

	public void setIntegrationStatusRecordCount(int integrationStatusRecordCount) {
		this.integrationStatusRecordCount = integrationStatusRecordCount;
	}

	public void setTestStatusRecordCount(int testStatusRecordCount) {
		this.testStatusRecordCount = testStatusRecordCount;
	}

	public void setUatStatusRecordCount(int uatStatusRecordCount) {
		this.uatStatusRecordCount = uatStatusRecordCount;
	}

	public void setLiveStatusRecordCount(int liveStatusRecordCount) {
		this.liveStatusRecordCount = liveStatusRecordCount;
	}

	public void setTotalRecordCountOfDepartment(int totalRecordCountOfDepartment) {
		this.totalRecordCountOfDepartment = totalRecordCountOfDepartment;
	}

	public String getOpentStatusRecordPercent() {
		return opentStatusRecordPercent;
	}

	public void setOpentStatusRecordPercent(String opentStatusRecordPercent) {
		this.opentStatusRecordPercent = opentStatusRecordPercent;
	}

	public String getInDevelopmentStatusPercent() {
		return inDevelopmentStatusPercent;
	}

	public void setInDevelopmentStatusPercent(String inDevelopmentStatusPercent) {
		this.inDevelopmentStatusPercent = inDevelopmentStatusPercent;
	}

	public String getUnitTestStatusPercent() {
		return unitTestStatusPercent;
	}

	public void setUnitTestStatusPercent(String unitTestStatusPercent) {
		this.unitTestStatusPercent = unitTestStatusPercent;
	}

	public String getFunctionalTestPercent() {
		return functionalTestPercent;
	}

	public void setFunctionalTestPercent(String functionalTestPercent) {
		this.functionalTestPercent = functionalTestPercent;
	}

	public String getInReviewPercent() {
		return inReviewPercent;
	}

	public void setInReviewPercent(String inReviewPercent) {
		this.inReviewPercent = inReviewPercent;
	}

	public String getIntegrationPercent() {
		return integrationPercent;
	}

	public void setIntegrationPercent(String integrationPercent) {
		this.integrationPercent = integrationPercent;
	}

	public String getTestPercent() {
		return testPercent;
	}

	public void setTestPercent(String testPercent) {
		this.testPercent = testPercent;
	}

	public String getUatPercent() {
		return uatPercent;
	}

	public void setUatPercent(String uatPercent) {
		this.uatPercent = uatPercent;
	}

	public String getLivePercent() {
		return livePercent;
	}

	public void setLivePercent(String livePercent) {
		this.livePercent = livePercent;
	}

}